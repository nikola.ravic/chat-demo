import React from 'react';
import PropTypes from 'prop-types';

const Button = ({disabled, onClick, text}) => {
  return (
    <button aria-label='Send message button' disabled={disabled} className='SendButton' onClick={onClick}>
      {text}
    </button>
  );
};

Button.propTypes = {
  disabled: PropTypes.bool,
  onClick: PropTypes.func,
  text: PropTypes.string
};

export { Button };
