import React from 'react';

const LoadingIndicator = () => {
  return (
    <div className='loader' />
  );
};
export { LoadingIndicator };
