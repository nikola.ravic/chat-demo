import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import ReactNotificationSystem from 'react-notification-system-redux';

const _ReactNotification = ({notifications}) => {
  return (
    <ReactNotificationSystem notifications={notifications} />
  );
};

_ReactNotification.propTypes = {
  notifications: PropTypes.array
};

const mapStateToProps = (state) => ({
  notifications: state.notifications
});

const ReactNotification = connect(mapStateToProps)(_ReactNotification);

export { ReactNotification };
