import React from 'react';
import PropTypes from 'prop-types';

const TextInput = ({value, onChange}) => {
  return (
    <input aria-label='Message Input' className='TextInput' onChange={onChange} placeholder='Message' value={value}/>
  );
};

TextInput.propTypes = {
  onChange: PropTypes.func,
  value: PropTypes.string
};

export { TextInput };
