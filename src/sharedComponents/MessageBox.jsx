import React from 'react';
import PropTypes from 'prop-types';

const MessageBox = ({timestamp, text, isSelfMessage, userName}) => {
  return (
    <div className={`MessageBox${isSelfMessage ? ' MessageBox__Self' : ''}`}>
      {userName && <span className='MessageBox__Username'>{userName}</span>}
      <span className='MessageBox__Text'>{text}</span>
      {timestamp && <span className='MessageBox__Timestamp'>{timestamp}</span>}
    </div>
  );
};

MessageBox.propTypes = {
  isSelfMessage: PropTypes.bool,
  timestamp: PropTypes.string,
  text: PropTypes.string,
  userName: PropTypes.string
};

export { MessageBox };
