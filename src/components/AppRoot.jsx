import React from 'react';
import { Provider } from 'react-redux';

import { ReactNotification } from 'SharedComponents';
import store from 'Redux/store';

import InputContainer from './InputContainer';
import MessagesContainer from './MessagesContainer';

const AppRoot = () => {
  return (
    <div className='ChatContainer'>
      <ReactNotification />
      <MessagesContainer />
      <InputContainer />
    </div>
  );
};

const AppRootWithProvider = () => (
  <Provider store={store}>
    <AppRoot />
  </Provider>
);

export default AppRootWithProvider;
