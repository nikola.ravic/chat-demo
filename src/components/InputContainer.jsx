import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { Button, TextInput } from 'SharedComponents';
import { sendMessage } from 'Actions';

export class InputContainer extends Component {
  static propTypes = {
    sendMessage: PropTypes.func,
    isLoading: PropTypes.bool
  }

  state = {
    isButtonDisabled: true,
    messageText: ''
  }

  // handles sending message to API
  handleSendMessage = () => {
    this.props.sendMessage(this.state.messageText, 'Nikola');
    this.setState({messageText: ''});
  }

  // updates state on text input
  onTextChange = (e) => {
    const messageText = e.target.value;
    this.setState({
      messageText
    });
  }

  render () {
    const {messageText} = this.state;
    const {isLoading} = this.props;

    return (
      <div className='InputBoxContainer'>
        <div className='InputBoxInnerContainer'>
          <TextInput value={messageText} onChange={this.onTextChange} />
          <Button onClick={this.handleSendMessage} text={isLoading ? 'Sending' : 'Send'} disabled={!messageText}
          />
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => ({
  sendMessage: (...args) => dispatch(sendMessage(...args))
});

const mapStateToProps = (state) => ({
  message: state.sendMessage.data,
  isLoading: state.sendMessage.isLoading
});

export default connect(mapStateToProps, mapDispatchToProps)(InputContainer);
