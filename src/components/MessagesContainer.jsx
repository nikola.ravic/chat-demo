import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { MessageBox, LoadingIndicator } from 'SharedComponents';
import { getAllMessages } from 'Actions';
import { momentHelper } from 'Util';

export class MessagesContainer extends Component {
  static propTypes = {
    isLoading: PropTypes.bool,
    getAllMessages: PropTypes.func,
    latestMessageTimestamp: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number
    ]),
    messages: PropTypes.array,
    sentMessage: PropTypes.object
  }

  componentDidMount = () => {
    this.props.getAllMessages();

    // fetches new messages every 10s
    this.fetchNewMessages = setInterval(() => this.props.getAllMessages({
      since: this.props.latestMessageTimestamp
    }), 10000);
  }

  // automatically scrolls to bottom if messages are updated
  scrollToBottom = (scrollBehavior) => {
    const messagesContainer = document.querySelector('.MessagesContainer');

    messagesContainer && messagesContainer.scrollTo({
      top: messagesContainer.scrollHeight,
      behavior: scrollBehavior
    });
  }

  componentDidUpdate (prevProps) {
    // scroll immediately to bottom after first fetch and smoothly every next update
    if (prevProps.messages.length !== 0 && prevProps.messages.length < this.props.messages.length) {
      this.scrollToBottom('smooth');
    } else if (prevProps.messages.length !== this.props.messages.length) {
      this.scrollToBottom('auto');
    }
  }

  render () {
    const {isLoading} = this.props;
    const {messages} = this.props;

    return (
      <div className='MessagesContainer'>
        {/* if request is fired we are showing loader, otherwise list of messages */}
        {isLoading
          ? <LoadingIndicator />
          : <div className='MessagesInnerContainer'>

            {messages && messages.map((message, i) => (
              <MessageBox
                isSelfMessage={message.author === 'Nikola' /* this is just for testing, it should be handled differently, with something unique for user using the app (token or else) */}
                key={message._id}
                userName={message.author}
                text={message.message}
                timestamp={momentHelper.formatMessagesTime(message.timestamp)}
              />
            ))}

          </div>
        }
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => ({
  getAllMessages: (...args) => dispatch(getAllMessages(...args))
});

const mapStateToProps = (state) => ({
  sentMessage: state.messages.sentMessage,
  messages: state.messages.data,
  newMessages: state.messages.newData,
  latestMessageTimestamp: state.messages.latestTimestamp,
  isLoading: state.messages.isLoading
});

export default connect(mapStateToProps, mapDispatchToProps)(MessagesContainer);
