import momentJS from 'moment';

const momentHelper = {
  formatMessagesTime: (timestamp) => momentJS(parseInt(timestamp)).format('DD MMM YYYY H:mm'),
  getCurrentTime: () => momentJS().valueOf()
};

export { momentHelper };
