// sorts array of objects by key provided
const sortByKey = (array, key) => array.sort((a, b) => {
  if (!a[key] || !b[key]) throw new Error(`There is no key ${key} in object provided.`);

  if (a[key] > b[key]) return 1;
  if (a[key] < b[key]) return -1;
  return 0;
});

export { sortByKey };
