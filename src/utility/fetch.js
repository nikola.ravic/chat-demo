import axios from 'axios';
import { error } from 'react-notification-system-redux';

import constants from 'Constants';

// wrapper around axios module to avoid repeating the code in actions
const fetch = ({method, url, params, data, requestAction, successAction, failureAction}) => {
  const config = {};

  if (method === 'get') {
    config.params = params || {};
    config.params.token = constants.AWESOME_TOKEN;
  } else {
    config.data = data;
    config.headers = {
      'Content-Type': 'application/json',
      'token': constants.AWESOME_TOKEN
    };
  }

  return (dispatch) => {
    if (requestAction) {
      dispatch(requestAction());
    }
    return axios({
      url: `${constants.baseUrl}${url || ''}`,
      method,
      ...config
    })
      .then(response => {
        successAction && dispatch(successAction(response.data));
      })
      .catch(err => {
        failureAction && dispatch(failureAction(err));

        dispatch(error({
          message: 'Something went wrong!' // error messages shouldn't be handled like this (once we know how API handled errors)
        }));
      });
  };
};

export { fetch };
