import React from 'react';
import ReactDOM from 'react-dom';
import AppRoot from 'Components/AppRoot';
import 'Styles';
import 'favicon';

ReactDOM.render(<AppRoot />, document.getElementById('root'));
