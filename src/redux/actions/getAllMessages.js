import { fetch } from 'Util';
import { actionTypes } from './actionTypes';

const requestAction = () => ({
  type: actionTypes.GET_MESSAGES_REQUEST
});

const successAction = (data) => ({
  type: actionTypes.GET_MESSAGES_SUCCESS,
  data
});

const getNewMessagesSuccessAction = (data) => ({
  type: actionTypes.GET_NEW_MESSAGES_SUCCESS,
  data
});

const getNewMessagesRequestAction = (data) => ({
  type: actionTypes.GET_NEW_MESSAGES_REQUEST,
  data
});

const failureAction = (error) => ({
  type: actionTypes.GET_MESSAGES_ERROR,
  error
});

const getAllMessages = (params = {}) =>
  fetch({
    method: 'get',
    params,
    requestAction: params.since ? getNewMessagesRequestAction : requestAction,
    successAction: params.since ? getNewMessagesSuccessAction : successAction,
    failureAction
  });

export { getAllMessages };
