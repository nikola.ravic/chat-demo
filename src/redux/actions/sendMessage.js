import { fetch } from 'Util';
import { actionTypes } from './actionTypes';

const requestAction = () => ({
  type: actionTypes.SEND_MESSAGE_REQUEST
});

const successAction = (data) => ({
  type: actionTypes.SEND_MESSAGE_SUCCESS,
  data
});

const failureAction = (error) => ({
  type: actionTypes.SEND_MESSAGE_ERROR,
  error
});

const sendMessage = (message, author) =>
  fetch({
    method: 'post',
    data: {
      message,
      author
    },
    requestAction,
    successAction,
    failureAction
  });

export { sendMessage };
