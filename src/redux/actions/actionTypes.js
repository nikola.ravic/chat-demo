import keyMirror from 'keymirror';

const actionTypes = keyMirror({
  GET_MESSAGES_REQUEST: null,
  GET_MESSAGES_SUCCESS: null,
  GET_NEW_MESSAGES_REQUEST: null,
  GET_NEW_MESSAGES_SUCCESS: null,
  GET_MESSAGES_ERROR: null,

  SEND_MESSAGE_REQUEST: null,
  SEND_MESSAGE_SUCCESS: null,
  SEND_MESSAGE_ERROR: null
});

export { actionTypes };
