import { combineReducers } from 'redux';

import messages from './messages';
import sendMessage from './sendMessage';
import { reducer as notifications } from 'react-notification-system-redux';

export default combineReducers({
  messages,
  sendMessage,
  notifications
});
