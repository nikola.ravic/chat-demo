import { actionTypes } from 'Actions';
import { momentHelper, sortByKey } from 'Util';
import unionBy from 'lodash.unionby';

const initialState = {
  isLoading: false,
  latestTimestamp: momentHelper.getCurrentTime(),
  data: [],
  error: undefined
};

export default (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.GET_MESSAGES_REQUEST:
      return {
        ...state,
        isLoading: true
      };
    case actionTypes.GET_MESSAGES_SUCCESS:
      return {
        ...state,
        isLoading: false,
        latestTimestamp: action.data && action.data.length && action.data[action.data.length - 1].timestamp,
        data: action.data
      };
    case actionTypes.GET_NEW_MESSAGES_SUCCESS:
      return {
        ...state,
        isLoading: false,
        latestTimestamp: action.data && action.data.length ? action.data[action.data.length - 1].timestamp : state.latestTimestamp,
        data: sortByKey(unionBy(state.data, action.data, '_id'), '_id') // we need to make sure that all messages are sorted by timestamp and to omit possible dupes
      };
    case actionTypes.GET_MESSAGES_ERROR:
      return {
        ...state,
        isLoading: false,
        error: action.error
      };
    case actionTypes.SEND_MESSAGE_SUCCESS:
      return {
        ...state,
        data: unionBy(state.data, [action.data], '_id')
      };
    default:
      return state;
  }
};
