import { actionTypes } from 'Actions';

const initialState = {
  isLoading: false,
  data: undefined,
  error: undefined
};

export default (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.SEND_MESSAGE_REQUEST:
      return {
        ...state,
        isLoading: true
      };
    case actionTypes.SEND_MESSAGE_SUCCESS:
      return {
        ...state,
        isLoading: false,
        error: undefined,
        data: action.data
      };
    case actionTypes.SEND_MESSAGE_ERROR:
      return {
        ...state,
        isLoading: false,
        data: undefined,
        error: action.error
      };
    default:
      return state;
  }
};
