import apiInfo from './apiInfo';
const {baseUrl, AWESOME_TOKEN} = apiInfo;
export default {
  baseUrl,
  AWESOME_TOKEN
};
