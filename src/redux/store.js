import {createStore, applyMiddleware, compose} from 'redux';
import rootReducer from 'Redux/reducers';
import thunk from 'redux-thunk';

const middlewares = [applyMiddleware(thunk)];

if (process.env.NODE_ENV === 'development' && window.__REDUX_DEVTOOLS_EXTENSION__) {
  middlewares.push(window.__REDUX_DEVTOOLS_EXTENSION__());
}

export default createStore(
  rootReducer,
  compose(...middlewares)
);
