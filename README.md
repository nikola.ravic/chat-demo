**Welcome to chat application demo!** 👏

This is a simple chat interface in JavaScript which is able to send messages
and display messages from all senders.

This awesome app is built up from zero, no boilerplate is used (like create-react-app or similar) as I like to know what my code is doing and it's working from the very beginning of the project. 🤔

I made a small effort and deployed the app on heroku. Also CD is set with gitlab so on every code push new app is built on heroku. You can check it out right now: https://chat-testapp.herokuapp.com/

I would you like to review the code and application and to share your thoughts with me. I would appreciate any suggestion. If you have any question please contact me.

To run this app use commands:

`yarn` (for the first time to install all deps)

**`yarn start`**

To build production ready code run:

**`yarn build`**

To run tests:

**`yarn test`**

or you can use `npm` if you like it. `start` command will run nodemon to watch for changes in webpack config files and webpack dev server will watch changes in your .js/.jsx/.css files.

I included eslint for code style checking (semistandard with accessibility support checks), tests for all components, precommit hook to run all tests and eslint check, in order to make sure that correct code will be commited and pushed.
Also I used autoprefixer to support 99.9% used browser across the web.
Small express server is included to serve static files on heroku.

Technologies used here: react, redux, redux-thunk, and others. Code is written in ES6+ and transpiled with babel. For testing I used mocha + enzyme + chai + sinon. I tried using jest but it has some bug related to the latest babel versions. I added notification for global errors handling, although I need to know error response data structure to be able to do this correctly (for testing purposes I hard-coded error message).
With API provided I setup fetching new data every 10s and merging to data already fetched. I was taking care of sorting messages according to timestamp.


What I would do more:

I wanted to lazy load messages but I didn't find the way with current informations given. If I got 'before' param (or something like that) instead of 'since' for fetching messages, it would be possible. I wanted to make 'Load more...' component and when user clicks/pulls that element to load more messages.
Also to be able to distinguish my own messages in UI from other users I needed additional info. For testing purposes I hard-coded that 'check' in MessagesContainer.
I would also like to write tests for redux part (actions, reducers)
And probably I would migrate backend logic to websockets to avoid "hacky" solutions in UI.

Thank you for your precious time! 🙏
