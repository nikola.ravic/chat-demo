Components
----------

**src/components/AppRoot.jsx**

### 1. AppRoot




### 2. AppRootWithProvider




-----
**src/components/InputContainer.jsx**

### 1. InputContainer




Property | Type | Required | Default value | Description
:--- | :--- | :--- | :--- | :---
sendMessage|func|no||
isLoading|bool|no||
-----
**src/components/MessagesContainer.jsx**

### 1. MessagesContainer




Property | Type | Required | Default value | Description
:--- | :--- | :--- | :--- | :---
isLoading|bool|no||
getAllMessages|func|no||
messages|array|no||
sentMessage|object|no||
-----
**src/sharedComponents/Button.jsx**

### 1. Button




Property | Type | Required | Default value | Description
:--- | :--- | :--- | :--- | :---
disabled|bool|no||
onClick|func|no||
text|string|no||
-----
**src/sharedComponents/LoadingIndicator.jsx**

### 1. LoadingIndicator




-----
**src/sharedComponents/MessageBox.jsx**

### 1. MessageBox




Property | Type | Required | Default value | Description
:--- | :--- | :--- | :--- | :---
isSelfMessage|bool|no||
timestamp|string|no||
text|string|no||
userName|string|no||
-----
**src/sharedComponents/ReactNotification.jsx**

### 1. _ReactNotification




Property | Type | Required | Default value | Description
:--- | :--- | :--- | :--- | :---
notifications|array|no||
-----
**src/sharedComponents/TextInput.jsx**

### 1. TextInput




Property | Type | Required | Default value | Description
:--- | :--- | :--- | :--- | :---
onChange|func|no||
value|string|no||
-----

<sub>This document was generated by the <a href="https://github.com/marborkowski/react-doc-generator" target="_blank">**React DOC Generator v1.2.5**</a>.</sub>
