const getWebpackPlugins = require('./webpack.plugins.js');
const rules = require('./webpack.rules.js');
const path = require('path');

module.exports = (env) => ({
  module: {
    rules: rules(env)
  },
  optimization: {
    splitChunks: {
      cacheGroups: {
        commons: {
          test: /[\\/]node_modules[\\/]/,
          name: 'vendor',
          chunks: 'initial'
        }
      }
    }
  },
  output: {
    path: path.join(__dirname, '../server/dist')
  },
  devtool: env.production ? 'source-map' : 'eval-source-map',
  plugins: getWebpackPlugins(),
  devServer: {
    stats: 'errors-only', // Display only errors to reduce the amount of output.
    host: process.env.HOST, // Defaults to `localhost`
    port: process.env.PORT, // Defaults to 8080
    open: true, // Open the page in browser,
    overlay: true // Displays errors in browser
  },
  resolve: {
    // adding aliases to avoid paths like ../../..
    alias: {
      Actions: path.resolve(__dirname, '../src/redux/actions'),
      Components: path.resolve(__dirname, '../src/components'),
      Constants: path.resolve(__dirname, '../src/redux/constants'),
      Redux: path.resolve(__dirname, '../src/redux'),
      SharedComponents: path.resolve(__dirname, '../src/sharedComponents'),
      Styles: path.resolve(__dirname, '../assets/styles'),
      favicon: path.resolve(__dirname, '../assets/images/favicon.ico'),
      Util: path.resolve(__dirname, '../src/utility')
    },
    extensions: ['.js', '.jsx', '.json', '.css']
  }
});
