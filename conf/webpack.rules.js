const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const rules = (env) => ([
  {
    test: /\.js$|\.jsx$/,
    exclude: /node_modules/,
    use: 'babel-loader'
  },
  {
    enforce: 'pre',
    test: /\.js$|\.jsx$/,
    exclude: /node_modules/,
    loader: 'eslint-loader'
  },
  {
    test: /\.ico$/,
    use: {
      loader: 'file-loader',
      options: {
        name: '[name].[ext]',
        publicPath: 'server/dist'
      }
    }
  },
  {
    test: /\.css$/,
    exclude: /node_modules/,
    use: [
      env.production
        ? MiniCssExtractPlugin.loader
        : 'style-loader',
      {
        loader: 'css-loader',
        options: {
          alias: {
            img: '../../assets/images'
          }
        }
      },
      {
        loader: 'postcss-loader',
        options: {
          plugins: () => [require('autoprefixer')()]
        }
      }
    ]
  },
  {
    test: /\.(png|jpg)$/,
    use: {
      loader: 'url-loader'
    }
  }
]);

module.exports = rules;
