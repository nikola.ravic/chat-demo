const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = function () {
  return [
    new HtmlWebpackPlugin({
      title: 'Simple chat application',
      template: 'app.html'
    }),
    new MiniCssExtractPlugin({
      filename: '[name].css'
    })
  ];
};
