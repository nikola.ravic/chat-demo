import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { expect } from 'chai';
import { spy } from 'sinon';

require('@babel/register')();

const jsdom = require('jsdom');
const { JSDOM } = jsdom;
const { window } = new JSDOM(`...`);
const { document } = window;

const exposedProperties = ['window', 'navigator', 'document'];

global.document = document;
global.navigator = { userAgent: 'node.js' };
global.window = window;
global.expect = expect;
global.spy = spy;

Object.keys(document.defaultView).forEach((property) => {
  if (typeof global[property] === 'undefined') {
    exposedProperties.push(property);
    global[property] = document.defaultView[property];
  }
});

Enzyme.configure({ adapter: new Adapter() });
