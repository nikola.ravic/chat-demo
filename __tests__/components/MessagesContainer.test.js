import React from 'react';
import { mount } from 'enzyme';
import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';

import { MessagesContainer } from 'Components/MessagesContainer';
import { MessageBox, LoadingIndicator } from 'SharedComponents';

const mockStore = configureStore([thunk]);

let wrapper;
const messages = [
  {
    text: 'test',
    timestamp: '10:30am',
    userName: 'test'
  },
  {
    text: 'test1',
    timestamp: '10:30am',
    userName: 'test1'
  },
  {
    text: 'test2',
    timestamp: '10:30am',
    userName: 'test2'
  }
];

describe('<MessagesContainer />', () => {
  before(() => {
    const props = {
      isLoading: true,
      messages: [],
      getAllMessages: () => {}
    };

    wrapper = mount(<MessagesContainer {...props} store={mockStore({messages: {}})} />);
  });

  it('should call render', () => {
    expect(wrapper.find('.MessagesContainer').length).to.equal(1);
  });

  it('should render loader if isLoading is passed', () => {
    wrapper.setProps({isLoading: true});

    expect(wrapper.find(LoadingIndicator).length).to.equal(1);
  });

  it('should render MessageBox elements when messages are passed', () => {
    wrapper.setProps({ isLoading: false, messages });

    expect(wrapper.find(MessageBox).length).to.equal(3);
  });
});
