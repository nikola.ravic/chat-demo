import React from 'react';
import { shallow } from 'enzyme';
import configureStore from 'redux-mock-store';

import AppRootWithProvider from 'Components/AppRoot';
import InputContainer from 'Components/InputContainer';
import MessagesContainer from 'Components/MessagesContainer';
import { ReactNotification } from 'SharedComponents';

const mockStore = configureStore();

let wrapper;

describe('<AppRoot />', () => {
  before(() => {
    wrapper = shallow(<AppRootWithProvider store={mockStore({})} />).dive();
  });

  it('should render', () => {
    expect(wrapper.length).to.equal(1);
  });

  it('should render all children', () => {
    expect(wrapper.dive().children().length).to.equal(3);
  });

  it('should render InputContainer', () => {
    expect(wrapper.dive().childAt(2).is(InputContainer)).to.equal(true);
  });

  it('should render MessagesContainer', () => {
    expect(wrapper.dive().childAt(1).is(MessagesContainer)).to.equal(true);
  });

  it('should render ReactNotification', () => {
    expect(wrapper.dive().childAt(0).is(ReactNotification)).to.equal(true);
  });
});
