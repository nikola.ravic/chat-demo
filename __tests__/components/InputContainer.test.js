import React from 'react';
import { mount } from 'enzyme';
import configureStore from 'redux-mock-store';

import InputContainer from 'Components/InputContainer';
import { TextInput, Button } from 'SharedComponents';

const mockStore = configureStore();

const spies = [];
let wrapper;

describe('<InputContainer />', () => {
  before(() => {
    spies.push(
      spy(InputContainer.prototype, 'componentDidMount'),
      spy(InputContainer.prototype, 'render')
    );

    wrapper = mount(<InputContainer store={mockStore({sendMessage: {}})}/>);
  });

  after(() => {
    spies.forEach(spy => spy.restore());
  });

  it('should render', () => {
    expect(InputContainer.prototype.render.calledOnce).to.equal(true);
  });

  it('should call componentDidMount', () => {
    expect(InputContainer.prototype.componentDidMount.calledOnce).to.equal(true);
  });

  it('should render all children', () => {
    expect(wrapper.find(Button).length).to.equal(1);
    expect(wrapper.find(TextInput).length).to.equal(1);
  });
});
