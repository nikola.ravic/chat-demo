import React from 'react';
import { shallow } from 'enzyme';

import { Button } from 'SharedComponents';

let wrapper;
let functionCallCounter = 0;
const onClick = () => { functionCallCounter++; }; // there is a bug in sinon.spy() with click event so this was the way

describe('<Button />', () => {
  before(() => {
    const props = {
      onClick,
      disabled: false,
      text: 'Horn!'
    };

    wrapper = shallow(<Button {...props} />);
  });

  it('should render button element', () => {
    expect(wrapper.find('button').length).to.equal(1);
  });

  it('should display correct text if passed as a prop', () => {
    expect(wrapper.find('button').text()).to.equal('Horn!');
  });

  it('should fire onClick function if clicked', () => {
    wrapper.simulate('click');

    expect(functionCallCounter).to.equal(1);

    wrapper.simulate('click');
    expect(functionCallCounter).to.equal(2);
  });

  it('should be disabled if disabled prop is passed', () => {
    wrapper.setProps({disabled: true});

    expect(wrapper.find('button').props().disabled).to.equal(true);
  });
});
