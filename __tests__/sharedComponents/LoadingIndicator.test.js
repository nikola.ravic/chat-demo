import React from 'react';
import { shallow } from 'enzyme';

import { LoadingIndicator } from 'SharedComponents';

let wrapper;

describe('<LoadingIndicator />', () => {
  before(() => {
    wrapper = shallow(<LoadingIndicator />);
  });

  it('should render correctly', () => {
    expect(wrapper.find('.loader').length).to.equal(1);
  });
});
