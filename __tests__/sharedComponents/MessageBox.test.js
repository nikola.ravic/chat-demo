import React from 'react';
import { shallow } from 'enzyme';

import { MessageBox } from 'SharedComponents';

let wrapper;

describe('<MessageBox />', () => {
  before(() => {
    const props = {
      text: 'Do you have a minute to talk about our lord GraphQl?',
      isSelfMessage: false
    };

    wrapper = shallow(<MessageBox {...props} />);
  });

  it('should render correctly all elements depending on passed props', () => {
    expect(wrapper.find('.MessageBox').length).to.equal(1);
    expect(wrapper.find('.MessageBox__Timestamp').length).to.equal(0);
    expect(wrapper.find('.MessageBox__Text').length).to.equal(1);
    expect(wrapper.find('.MessageBox__Username').length).to.equal(0);
  });

  it('should render correctly styled message of user if isSelfMessage prop is passed', () => {
    wrapper.setProps({isSelfMessage: true});

    expect(wrapper.find('.MessageBox__Self').length).to.equal(1);
  });

  it('should render username if prop is passed', () => {
    wrapper.setProps({userName: 'Deadpool'});

    expect(wrapper.find('.MessageBox__Username').length).to.equal(1);
    expect(wrapper.find('.MessageBox__Username').text()).to.equal('Deadpool');
  });

  it('should render timestamp if prop is passed', () => {
    wrapper.setProps({timestamp: '22.05.1984'});

    expect(wrapper.find('.MessageBox__Timestamp').length).to.equal(1);
    expect(wrapper.find('.MessageBox__Timestamp').text()).to.equal('22.05.1984');
  });

  it('should render correct message text', () => {
    expect(wrapper.find('.MessageBox__Text').text()).to.equal('Do you have a minute to talk about our lord GraphQl?');
  });
});
