import React from 'react';
import { mount } from 'enzyme';
import configureStore from 'redux-mock-store';

import { ReactNotification } from 'SharedComponents';

const mockStore = configureStore({});

describe('<ReactNotification />', () => {
  before(() => {
    spy(ReactNotification.prototype, 'render');

    mount(<ReactNotification store={mockStore({})} />);
  });

  it('should render correctly', () => {
    expect(ReactNotification.prototype.render.calledOnce).to.equal(true);
  });
});
