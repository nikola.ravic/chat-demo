import React from 'react';
import { shallow } from 'enzyme';

import { TextInput } from 'SharedComponents';

let wrapper;

describe('<TextInput />', () => {
  before(() => {
    const onChange = () => { };
    const props = {
      onChange
    };

    spy(onChange);

    wrapper = shallow(<TextInput {...props} />);
  });

  it('should render input element', () => {
    expect(wrapper.find('input').length).to.equal(1);
  });

  it('should display correct value if passed as a prop', () => {
    wrapper.setProps({ value: 'test' });
    expect(wrapper.find('input').props().value).to.equal('test');
  });

  it('should fire onChange function if text is entered', () => {
    const onChange = spy();

    wrapper.setProps({onChange});

    wrapper.simulate('change', { target: { value: 'd' } });
    wrapper.simulate('change', { target: { value: 'do' } });

    expect(wrapper.find('input').props().onChange.callCount).to.equal(2);
  });
});
