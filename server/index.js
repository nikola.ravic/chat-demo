const express = require('express');
const app = express();

app.use(express.static('server/dist'));
app.get('*', (req, res) => res.send('/'));
app.set('port', (process.env.PORT || 3333));

app.listen(app.get('port'), () => {
  console.log(`Server started on port ${app.get('port')}`);
});
